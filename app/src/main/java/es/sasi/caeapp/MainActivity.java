package es.sasi.caeapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import es.sasi.caeapp.model.Transformation;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TransformationsPlay t = TransformationsPlay.getInstance(loadTransformationsFromFile());
        t.reset();
    }

    public void startTransformations(View view) {
//        Intent intent = new Intent(this, TransformationsActivity.class);
        Intent intent = new Intent(this, TransformationsInfoActivity.class);
        startActivity(intent);
    }

    public ArrayList<Transformation> loadTransformationsFromFile() {
        ArrayList<Transformation> listT = new ArrayList<Transformation>();
        Scanner sc = null;
        try {
            String fileName = "transformationsTextText";
//            sc = new Scanner(new File(fileName));
            sc = new Scanner(getResources().openRawResource(R.raw.transformations_text));
            System.out.println("Reading file: " + fileName);
        } catch (Exception e) {

        }

        int i = 1;
        while (sc.hasNextLine()) {
            String line = sc.nextLine();

            if (line.startsWith("##")) {
                // System.out.println("Using = " + line.substring(2, line.length()));
            } else if (line.startsWith("#")) {
                // System.out.println("    " + line.substring(1, line.length()));
            } else {
                if (!line.isEmpty()) {
                    String original = line;
                    String word = sc.nextLine();
                    String solution = sc.nextLine();

                    Transformation t = new Transformation(original, word, solution);
                    listT.add(t);
                    /*System.out.println(i++ + ". " + t.getOriginalSentence());
                    System.out.println(t.getWord());
					System.out.println(t.getAlternativeSentence());
					System.out.println();*/
                }
            }
        }

        Collections.shuffle(listT);
        return listT;
    }
}
