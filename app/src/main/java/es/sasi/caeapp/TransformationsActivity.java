package es.sasi.caeapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import es.sasi.caeapp.model.Transformation;

public class TransformationsActivity extends AppCompatActivity {

    private TransformationsPlay transformationsPlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transformations);

//        ArrayList<Transformation> transformationArrayList = Transformation.createSome();

//        t = transformationArrayList.get(0);
        transformationsPlay = TransformationsPlay.getInstance();

        Transformation t = transformationsPlay.getNextTransformation();

        TextView text = (TextView) findViewById(R.id.textOriginal);
        text.setText(t.getOriginal());

        text = (TextView) findViewById(R.id.textAlternative);
        text.setText(t.getAlternative());

        text = (TextView) findViewById(R.id.textWord);
        text.setText(t.getWord());
    }

    public void checkAnswer(View view) {

        // Do something in response to button
        EditText edit = (EditText) findViewById(R.id.editSolution);
//        int points = transformationsPlay.getLastTransformation().checkSolution(edit.getText().toString());

//        transformationsPlay.updatePoints(points);

        transformationsPlay.checkSolution(edit.getText().toString());

        Intent intent = new Intent(this, TransformationsInfoActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, TransformationsSummary.class);
        startActivity(intent);
    }
}
