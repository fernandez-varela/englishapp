package es.sasi.caeapp;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import es.sasi.caeapp.model.Transformation;
import es.sasi.caeapp.model.TransformationSolution;

public class TransformationsInfoActivity extends AppCompatActivity {

//    private int actualPoints;
//    private int possiblePoints;

    private TransformationsPlay transformationsPlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transformations_info);

        transformationsPlay = TransformationsPlay.getInstance();

        int actualPoints = transformationsPlay.getObtainedPoints();
        int possiblePoints = transformationsPlay.getSolvedTransformations() * 2;
        int percentage = (possiblePoints == 0) ? 0 : actualPoints * 100 / possiblePoints;

        TextView text = (TextView) findViewById(R.id.textPoints);
        text.setText(actualPoints + " / " + possiblePoints + " (" + percentage + " %)");

        Button button = (Button) findViewById(R.id.buttonNextTransformation);

        text = (TextView) findViewById(R.id.textOriginal);
        text.setText("");

        text = (TextView) findViewById(R.id.textWord);
        text.setText("");

        text = (TextView) findViewById(R.id.textSolution);
        text.setText("");

        text = (TextView) findViewById(R.id.textUserSolution);
        text.setText("");

        if (possiblePoints == 0) {
            button.setText("Start");

            text = (TextView) findViewById(R.id.textOriginal);
            text.setText("Are you ready to play?");

        } else {
            button.setText("Next one");
            int lastPoints = transformationsPlay.getLastPoints();
            Transformation t = transformationsPlay.getLastTransformation();

            text = (TextView) findViewById(R.id.textResult);
            if (lastPoints == 2) {
                text.setText("Good answer! :)");
                text.setTextColor(Color.GREEN);
            } else {
                text.setText("Wrong answer! :(");
                text.setTextColor(Color.RED);

                text = (TextView) findViewById(R.id.textOriginal);
                text.setText(t.getOriginal());

                showSolution(t);


            }
        }
    }

    public void showSolution(Transformation t) {

        TextView text = (TextView) findViewById(R.id.textSolution);
//        String s = t.getCompleteSolution();
        String s = t.getSimilarSolution(transformationsPlay.getLastSolution());
        String w = t.getWord();
        int idxw = s.indexOf(w.toLowerCase());
        s = s.substring(0, idxw) + w + s.substring(idxw + w.length());

        int[] idx = t.getSolutionIndex();
        SpannableStringBuilder sb = new SpannableStringBuilder(s);
        StyleSpan iss = new StyleSpan(Typeface.ITALIC);
        sb.setSpan(iss, idx[0], s.length() - idx[1], Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        text.setText(sb);

        text = (TextView) findViewById(R.id.textUserSolution);
        String s1 = "Your solution was: ";
        s = s1 + transformationsPlay.getLastSolution();
        sb = new SpannableStringBuilder(s);
        iss = new StyleSpan(android.graphics.Typeface.ITALIC);
        sb.setSpan(iss, s1.length(), s.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        text.setText(sb);

        FloatingActionButton b = (FloatingActionButton) findViewById(R.id.buttonSeeSolutions);

        if (t.hasMultipleSolutions()) {
            b.setVisibility(View.VISIBLE);
        } else {
            b.setVisibility(View.INVISIBLE);
        }

        ViewGroup layout = (ViewGroup) findViewById(R.id.LinearLayout);
        layout.setVisibility(View.INVISIBLE);
    }

    public void showAllSolutions(View view) {
        FloatingActionButton b = (FloatingActionButton) view;

        ViewGroup layout = (ViewGroup) findViewById(R.id.LinearLayout);

        if (layout.getVisibility() == View.INVISIBLE) {
            Transformation t = transformationsPlay.getLastTransformation();


            ArrayList<TransformationSolution> solutionList = t.getSolutionList();
            String w = t.getWord();

            for (TransformationSolution s : t.getSolutionList()) {
                String solution = s.getSolutionBegining() + " " + s.getSolutionEnding();

                int idxw = solution.indexOf(w.toLowerCase());
                solution = solution.substring(0, idxw) + w + solution.substring(idxw + w.length());

                TextView textView = new TextView(this);
                textView.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                textView.setTypeface(null, Typeface.ITALIC);
                textView.setText(solution);

                layout.addView(textView);
            }
            layout.setVisibility(View.VISIBLE);
//            b.setText("-");
        } else {
            layout.removeAllViews();
            layout.setVisibility(View.INVISIBLE);
//            b.setText("+");
        }
    }

    public void nextTransformation(View view) {
        Intent intent = new Intent(this, TransformationsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, TransformationsSummary.class);
        startActivity(intent);
    }
}
