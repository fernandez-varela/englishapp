package es.sasi.caeapp;

import java.util.ArrayList;
import java.util.Collections;

import es.sasi.caeapp.model.Transformation;

/**
 * Created by sasi on 24/02/17.
 */

public class TransformationsPlay {
    private static TransformationsPlay instance;

    private int obtainedPoints;
    private int solvedTransformations;
    private ArrayList<Transformation> availableTransformations;
    private ArrayList<Transformation> failedTranformations;
    private Transformation lastTransformation;
    private int lastPoints;
    private String lastSolution;

    public int getLastPoints() {
        return lastPoints;
    }

    public static TransformationsPlay getInstance() {
        /*if (instance == null) {
            instance = new TransformationsPlay();
        }*/

        return instance;
    }

    public static TransformationsPlay getInstance(ArrayList<Transformation> availableTransformations) {
        if (instance == null) {
            instance = new TransformationsPlay(availableTransformations);
        }

        return instance;
    }

    private TransformationsPlay(ArrayList<Transformation> availableTransformations) {
        obtainedPoints = 0;
        solvedTransformations = 0;
//        availableTransformations = new ArrayList<Transformation>();
        failedTranformations = new ArrayList<Transformation>();

        this.availableTransformations = availableTransformations;
    }

    public void reset() {
        obtainedPoints = 0;
        solvedTransformations = 0;
        Collections.shuffle(availableTransformations);
        failedTranformations = new ArrayList<Transformation>();
    }
    /*public static ArrayList<Transformation> loadTransformationsFromFile() {
        ArrayList<Transformation> listT = new ArrayList<Transformation>();
        Scanner sc = null;
        try {
            String fileName = "transformationsTextText";
            sc = new Scanner(new File(fileName));
            System.out.println("Reading file: " + fileName);
        } catch (Exception e) {

        }

        int i = 1;
        while (sc.hasNextLine()) {
            String line = sc.nextLine();

            if (line.startsWith("##")) {
                // System.out.println("Using = " + line.substring(2, line.length()));
            } else if (line.startsWith("#")) {
                // System.out.println("    " + line.substring(1, line.length()));
            } else {
                if (!line.isEmpty()) {
                    String original = line;
                    String word = sc.nextLine();
                    String solution = sc.nextLine();

                    Transformation t = new Transformation(original, word, solution);
                    listT.add(t);
					*//*System.out.println(i++ + ". " + t.getOriginalSentence());
                    System.out.println(t.getWord());
					System.out.println(t.getAlternativeSentence());
					System.out.println();*//*
                }
            }
        }

        Collections.shuffle(listT);
        return listT;
    }*/

    public Transformation getNextTransformation() {
        lastTransformation = availableTransformations.get(solvedTransformations);
        return lastTransformation;
    }

    public void checkSolution(String solution) {
        lastPoints = lastTransformation.checkSolution(solution);
        if (lastPoints == 0)
            failedTranformations.add(lastTransformation);

        solvedTransformations++;
        obtainedPoints += lastPoints;
        lastSolution = solution;
    }

/*    public void updatePoints(int points) {
        if (points == 0)
            failedTranformations.add(lastTransformation);
        obtainedPoints += points;
        solvedTransformations++;
        lastPoints = points;
    }*/

    public ArrayList<Transformation> getFailedTranformations() {
        return failedTranformations;
    }

    public int getObtainedPoints() {
        return obtainedPoints;
    }

    public int getSolvedTransformations() {
        return solvedTransformations;
    }

    public ArrayList<Transformation> getAvailableTransformations() {
        return availableTransformations;
    }

    public Transformation getLastTransformation() {
        return lastTransformation;
    }

    public String getLastSolution() {
        return lastSolution;
    }
}
