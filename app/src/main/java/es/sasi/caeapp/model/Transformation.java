package es.sasi.caeapp.model;

import java.util.ArrayList;

/**
 * Created by sasi on 16/02/17.
 */

public class Transformation {

    private long transformationId;
    private String original;
    private String word;
    private String alternativeBeginning;
    private String alternativeEnding;

    public ArrayList<TransformationSolution> getSolutionList() {
        return solutionList;
    }

    private ArrayList<TransformationSolution> solutionList;

    public static ArrayList<Transformation> createSome() {
        ArrayList<Transformation> transformationArrayList = new ArrayList<Transformation>();

        TransformationSolution sol = new TransformationSolution("keeping", "an eye on");
        Transformation t1 = new Transformation("Could you watch my suitcases while I go and buy my ticket?", "EYE", "Would you mind", "my suitcases while I go and buy my ticket?", sol);
        sol = new TransformationSolution("to be", "higher than it");
        Transformation t2 = new Transformation("The rainfall in the wet of the country usually exceeds that in the east.", "HIGHER", "The rainfaill in the west of the country tends", "is in the east", sol);

        transformationArrayList.add(t1);
        transformationArrayList.add(t2);

        return transformationArrayList;
    }

    public Transformation(String sentence, String word, String alternativeBeginning, String alternativeEnding, TransformationSolution solution) {
        original = sentence;
        this.word = word;
        this.alternativeBeginning = alternativeBeginning;
        this.alternativeEnding = alternativeEnding;

        solutionList = new ArrayList<TransformationSolution>();
        solutionList.add(solution);
    }

    public Transformation(String original, String word, String alternativeWithSolution) {
        this.original = original;
        this.word = word;

        int i1 = alternativeWithSolution.indexOf('[');
        int i2 = alternativeWithSolution.indexOf(']');

        String alternativeBeginning = alternativeWithSolution.substring(0, i1 - 1);
        String alternativeEnding = alternativeWithSolution.substring(i2 + 2, alternativeWithSolution.length());
        String allSolutions = alternativeWithSolution.substring(i1 + 1, i2);

        this.alternativeBeginning = alternativeBeginning;
        this.alternativeEnding = alternativeEnding;

        solutionList = new ArrayList<TransformationSolution>();

        String[] solutions = allSolutions.split(";");
        for (int i = 0; i < solutions.length; i++) {
            String tmp = solutions[i].trim();
            int idx = tmp.indexOf('|');
            String solBeginning = tmp.substring(0, idx - 1);
            String solEnding = tmp.substring(idx + 2, tmp.length());
            TransformationSolution sol = new TransformationSolution(solBeginning, solEnding);
            solutionList.add(sol);
        }
    }

    public String getAlternative() {
        return alternativeBeginning + " ..... " + alternativeEnding;
    }

    public TransformationSolution getSolution() {
        return solutionList.get(0);
    }

    public String getCompleteSolution() {
        return alternativeBeginning + " " + getSolution() + " " + alternativeEnding;
    }

    public String getSimilarSolution(String proposedSolution) {
        TransformationSolution solution = solutionList.get(0);
        int maxSimilarity = 0;
        for (TransformationSolution ts : solutionList) {
            int similarity = pecentageOfTextMatch(proposedSolution, ts.toString());
            if (similarity > maxSimilarity) {
                maxSimilarity = similarity;
                solution = ts;
            }
        }
        return alternativeBeginning + " " + solution.toString() + " " + alternativeEnding;
    }

    public int[] getSolutionIndex() {

        return new int[]{alternativeBeginning.length(), alternativeEnding.length()};
    }

    public String getOriginal() {
        return original;
    }

    public String getWord() {
        return word;
    }

    public int checkSolution(String solutionString) {
        int points = 0;
        for (TransformationSolution s : solutionList) {
            int actualPoints = s.checkSolution(solutionString);
            if (actualPoints == 2) {
                points = 2;
                break;
            } else if (actualPoints == 1) {
                points = 1;
            }
        }
        return points;
    }

    public boolean hasMultipleSolutions() {
        return solutionList.size() > 1;
    }

    public static int pecentageOfTextMatch(String s0, String s1) {                       // Trim and remove duplicate spaces
        int percentage = 0;
        s0 = s0.trim().replaceAll("\\s+", " ");
        s1 = s1.trim().replaceAll("\\s+", " ");
        percentage = (int) (100 - (float) LevenshteinDistance(s0, s1) * 100 / (float) (s0.length() + s1.length()));
        return percentage;
    }

    public static int LevenshteinDistance(String s0, String s1) {

        int len0 = s0.length() + 1;
        int len1 = s1.length() + 1;
        // the array of distances
        int[] cost = new int[len0];
        int[] newcost = new int[len0];

        // initial cost of skipping prefix in String s0
        for (int i = 0; i < len0; i++)
            cost[i] = i;

        // dynamically computing the array of distances

        // transformation cost for each letter in s1
        for (int j = 1; j < len1; j++) {

            // initial cost of skipping prefix in String s1
            newcost[0] = j - 1;

            // transformation cost for each letter in s0
            for (int i = 1; i < len0; i++) {

                // matching current letters in both strings
                int match = (s0.charAt(i - 1) == s1.charAt(j - 1)) ? 0 : 1;

                // computing cost for each transformation
                int cost_replace = cost[i - 1] + match;
                int cost_insert = cost[i] + 1;
                int cost_delete = newcost[i - 1] + 1;

                // keep minimum cost
                newcost[i] = Math.min(Math.min(cost_insert, cost_delete),
                        cost_replace);
            }

            // swap cost/newcost arrays
            int[] swap = cost;
            cost = newcost;
            newcost = swap;
        }

        // the distance is the cost for transforming all letters in both strings
        return cost[len0 - 1];
    }
}
