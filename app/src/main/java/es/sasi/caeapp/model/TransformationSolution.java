package es.sasi.caeapp.model;

/**
 * Created by sasi on 24/02/17.
 */

public class TransformationSolution {
    private String solutionBegining;
    private String solutionEnding;

    public TransformationSolution(String solutionBegining, String solutionEnding) {
        this.solutionBegining = solutionBegining;
        this.solutionEnding = solutionEnding;
    }

    public String getSolutionBegining() {
        return solutionBegining;
    }

    public String getSolutionEnding() {
        return solutionEnding;
    }

    public int checkSolution(String possibleSolution) {
        int points = 0;
        if (possibleSolution.startsWith(solutionBegining))
            points++;
        if (possibleSolution.endsWith(solutionEnding))
            points++;

        if (points == 2) {
            if (!possibleSolution.equalsIgnoreCase(toString()))
                points--;
        }

        return points;
    }

    public String toString() {
        return solutionBegining + " " + solutionEnding;
    }
}
